//
//  PKAppDelegate.h
//  Grill Timer
//
//  Created by Micah Grigonis on 10/17/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
