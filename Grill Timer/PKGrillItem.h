//
//  PKGrillItem.h
//  Grill Timer
//
//  Created by Micah Grigonis on 11/7/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PKGrillItemStatus;

@interface PKGrillItem : NSObject
{

    NSString *          _name;
    CGImageRef          _picture;
    NSMutableArray *    _flipsAndChecks;
    int                 _totalTime;
    
}

- (int) totalTime;
- (void) setTotalTime: (int) inTime;

- (NSString *) name;
- (void) setName: (NSString*) inName;

- (NSMutableArray *) flipsAndChecks;


- (void) startGrillingItem: (NSDate *)inGrillItemStartTime;
- (void) cancelGrillingItem;



@end
