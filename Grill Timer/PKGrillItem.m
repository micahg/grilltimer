//
//  PKGrillItem.m
//  Grill Timer
//
//  Created by Micah Grigonis on 11/7/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import "PKGrillItem.h"

#import "PKGrillItemStatus.h"

@implementation PKGrillItem

- (id) init
{
	self = [super init];
	
	if (self)
	{
        //NEED TO LOAD THE FLIPS AND CHECKS HERE AS WELL...
		_flipsAndChecks = [NSMutableArray arrayWithCapacity: 10];
	}
	
	return self;
}

- (int) totalTime
{
    return _totalTime;
}

- (void) setTotalTime: (int) inTime
{
    _totalTime = inTime;
}

- (NSString *) name
{
    return _name;
}

- (void) setName: (NSString*) inName
{
	_name = inName;
}


- (NSMutableArray *) flipsAndChecks
{
    return _flipsAndChecks;
}


- (void) startGrillingItem: (NSDate *)inGrillItemStartTime
{
    PKGrillItemStatus * status = nil;
    
    for (int i = 0; i < [_flipsAndChecks count]; i++ )
    {
        status = [_flipsAndChecks objectAtIndex: i];
        [status startItemStatus:inGrillItemStartTime];
    }
}


- (void) cancelGrillingItem
{
    PKGrillItemStatus * status = nil;
    
    for (int i = 0; i < [_flipsAndChecks count]; i++ )
    {
        status = [_flipsAndChecks objectAtIndex: i];
        [status cancelItemStatus];
    }
}

@end
