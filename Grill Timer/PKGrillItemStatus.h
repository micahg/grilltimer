//
//  PKGrillItemStatus.h
//  Grill Timer
//
//  Created by Micah Grigonis on 11/7/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
typedef enum
{
    kGrillPutOnGrill = 0,
	kGrillFlip		= 1,
	kGrillCheck		= 2,
	kGrillLowerHeat	= 3,
	kGrillRaiseHeat	= 4,
    kGrillMove      = 5,
    kGrillRest      = 6,
    kGrillEat       = 7
	
} EGrillItemStatusType;
*/

@interface PKGrillItemStatus : NSObject
{
    NSString *              _statusString;
    
    //time relative to the start of the Grill  item
    int                     _time;
    int                     _temp;
    bool                    _fired;
    UILocalNotification     *_itemNotification;
}

- (NSString *) statusString;
- (void) setStatusString: (NSString *) inStatusString;

- (int) time;
- (void) setTime: (int) inTime;

- (int) temp;
- (void) setTemp: (int) inTemp;

- (bool) fired;
- (void) setFired;

- (void) startItemStatus: (NSDate *) inGrillItemStartTime;
- (void) cancelItemStatus;

@end
