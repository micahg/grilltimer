//
//  PKGrillItemStatus.m
//  Grill Timer
//
//  Created by Micah Grigonis on 11/7/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import "PKGrillItemStatus.h"

@implementation PKGrillItemStatus

- (id) init
{
	self = [super init];
	
	if (self)
	{
		_fired = false;
        _itemNotification = [[UILocalNotification alloc] init];
        _time = -1;
	}
	
	return self;
}


- (NSString *) statusString
{
    return _statusString;
}

- (void) setStatusString: (NSString *) inStatusString
{
    _statusString = inStatusString;
}

- (int) time
{
    return _time;
}

- (void) setTime: (int) inTime
{
    _time = inTime;
}

- (int) temp
{
    return _temp;
}

- (void) setTemp: (int) inTemp
{
    _temp = inTemp;
}

- (bool) fired
{
    return _fired;
}

- (void) setFired
{
    _fired = true;
}


- (void) startItemStatus: (NSDate *) inGrillItemStartTime
{
    if (_time != -1)
    {
    
        _itemNotification.fireDate = [inGrillItemStartTime dateByAddingTimeInterval:_time];
        _itemNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        _itemNotification.alertBody = _statusString;
        _itemNotification.alertAction = NSLocalizedString(@"Ok", nil);
        
        _itemNotification.soundName = UILocalNotificationDefaultSoundName;
        _itemNotification.applicationIconBadgeNumber = 1;
        
        
        [[UIApplication sharedApplication] scheduleLocalNotification:_itemNotification];
    }
}

- (void) cancelItemStatus
{
    [[UIApplication sharedApplication] cancelLocalNotification:_itemNotification];
}









/*
- (NSString *) getTypeString
{
    NSMutableString *stringtoReturn = [[NSMutableString	alloc] init];

    
	switch ([self type]) {
		case kGrillFlip:
			[stringtoReturn appendString: NSLocalizedString(@"Flip ",@"")];
			break;
		case kGrillCheck:
			[stringtoReturn appendString: NSLocalizedString(@"Check ",@"")];
			break;
		case kGrillLowerHeat:
			[stringtoReturn appendString: NSLocalizedString(@"Lower Heat ",@"")];
			break;
		case kGrillRaiseHeat:
			[stringtoReturn appendString: NSLocalizedString(@"Raise Heat ",@"")];
			break;
        case kGrillMove:
			[stringtoReturn appendString: NSLocalizedString(@"Move to Top Rack ",@"")];
            break;
        case kGrillRest:
			[stringtoReturn appendString: NSLocalizedString(@"Set aside to Rest ",@"")];
            break;
        case kGrillEat:
			[stringtoReturn appendString: NSLocalizedString(@"Enjoy! ",@"")];
            break;
            
		default:
			//oops
			break;
	}
	
	return stringtoReturn;
}
 */

@end
