//
//  main.m
//  Grill Timer
//
//  Created by Micah Grigonis on 10/17/12.
//  Copyright (c) 2012 Micah Grigonis. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PKAppDelegate class]));
    }
}
